#!/usr/bin/env python

# use export PS1='$(/Users/andrey/prj/prompt-py/prompt-build.py $?)' in .bashrc

import os
import sys
from subprocess import Popen, PIPE

def print_prompt():

    prev_exit_code = "0" 
    if len(sys.argv) > 1:
        prev_exit_code = sys.argv[1]

    pwd = os.environ['PWD']
    home = os.environ['HOME']
    user = os.environ['USER']

    yellow_color = "\\[\x1b[0;33m\\]"
    reset_color = "\\[\x1b[0m\\]"

    # status
    # git status --untracked-files=all --porcelain --branch
    # in repo
    # git rev-parse --git-dir
    cmd = "git status --untracked-files=all --porcelain --branch"

    try:
        process = Popen(cmd.split(), stdout=PIPE, stderr=PIPE)
        (output, err) = process.communicate()
        exit_code = process.wait()
        outstrings = [s.strip(b"\n") for s in output.split(b"\n")]
    except Exception:
        outstrings = []
        exit_code = -128

    if exit_code == 0:
        prompt = ">" + prev_exit_code + ">" + yellow_color + pwd + reset_color + " $ "
    else:
        prompt = pwd + " $ "        

    print (prompt)

    # print (sys.version_info)


def main():
    print_prompt()

if __name__ == '__main__':
    main()
