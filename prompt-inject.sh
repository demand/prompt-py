#!/usr/bin/env bash

# --------
# Add follofing line to .bashrc file to activate prompt-py:
# source "path/to/script/prompt-inject.sh"
# --------

# code was taken from http://stackoverflow.com/questions/59895
function prompt_dir {
    local SOURCE="${BASH_SOURCE[0]}"
    while [ -h "$SOURCE" ]; do
      local DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
      SOURCE="$(readlink "$SOURCE")"
      [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE"
    done
    PROMPT_PY_DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
}

function prepare_prompt_py {
    local LAST_CODE=$?
    prompt_dir
    PS1="$($PROMPT_PY_DIR/prompt-build.py $LAST_CODE)"
}

export PROMPT_COMMAND=prepare_prompt_py
